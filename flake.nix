{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-parts,
    devshell,
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = nixpkgs.lib.systems.flakeExposed;

      imports = [
        inputs.devshell.flakeModule
      ];

      perSystem = {
        config,
        pkgs,
        system,
        ...
      }: {
        formatter = pkgs.alejandra;

        packages.default = import ./default.nix {inherit pkgs;};

        devshells.default = {
          packages = [
            (pkgs.python3.withPackages (p:
              with p; [
                pip
                build
                twine
              ]))
          ];

          commands = [
            {
              name = "install";
              command = ''
                python -m pip install -e .
              '';
            }
            {
              name = "build";
              command = ''
                rm -rfv dist/*
                python -m build
                python -m twine check dist/*
              '';
            }
            {
              name = "upload";
              command = ''
                python -m twine upload -u __token__ dist/*
              '';
            }
          ];
        };
      };
    };
}
