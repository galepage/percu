"""
RemI, a cli tool for remotely interacting with Inria computing resources.
"""
from .cli import remi
